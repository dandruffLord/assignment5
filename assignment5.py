#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '0'}, \
		 {'title': 'Algorithm Design', 'id':'1'}, \
		 {'title': 'Python', 'id':'2'}]

@app.route('/book/JSON/')
def bookJSON():
    return jsonify({'books': books})
    # <your code>

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books=books)
    # <your code>	

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    book = next(book for book in books if book['id'] == str(book_id))
    if request.method == 'POST':
        book['title'] = request.form['name']
        return redirect(url_for('showBook'))
    return render_template('editBook.html', book=book)
    # <your code>

@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    book = next(book for book in books if book['id'] == str(book_id))
    if request.method == 'POST':
        books.remove(book)
        return redirect(url_for('showBook'))
    return render_template('deleteBook.html', book=book)
    # <your code>    

@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        books.append({
            'title': request.form['name'], 
            'id': str(next((id for id in range(len(books)) if books[id]['id'] != str(id)), len(books)))
        })
        return redirect(url_for('showBook'))
    return render_template('newBook.html')
    # <your code>

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

